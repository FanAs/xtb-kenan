const http = require('http');
const WebSocket = require('ws');
require('dotenv').config();

const server = http.createServer((req, res) => {
	if (req.url !== `/${process.env.UNIQUE_HASH}`) {
		res.end('fuck you');
		return;
	}

	const timeoutId = setTimeout(() => {
		res.end('sorry :(');
		return;
	}, 10000);

	const s = new WebSocket(process.env.SOCKET_MAIN_URI);
	const sReal = new WebSocket(process.env.SOCKET_STREAM_URI);

	const sPromise = new Promise((resolve) => s.on('open', () => resolve()));
	const sRealPromise = new Promise((resolve) => sReal.on('open', () => resolve()));

	Promise.all([sPromise, sRealPromise]).then(() => {
		s.send(
			JSON.stringify({
				command: 'login',
				arguments: {
					userId: process.env.USER_ID,
					password: process.env.PASSWORD,
				}
			})
		)
	});

	sReal.on('message', (response) => {
		clearTimeout(timeoutId);
		const { data } = JSON.parse(response);

		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(data));

		s.send(JSON.stringify({
			command: 'logout',
		}));

		s.close();

		sReal.send(JSON.stringify({
			command: 'logout',
		}));

		sReal.close();
	})

	s.on('message', (data) => {
		const {streamSessionId} = JSON.parse(data);
		sReal.send(
			JSON.stringify({
				command: 'getBalance',
				streamSessionId,
			})
		)
	})
});

server.listen(Number(process.env.SERVER_PORT));
